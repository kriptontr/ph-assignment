window.bubbles = (function () {
    var self = this;
    var xMultipier = 2,
        yMultipier = 3;
    var canvas,ctx;
    self.init = function (selector, data) {
        var parentEl = document.querySelector(selector);
        if (!parentEl) {
            alert("Cant find container element described in init selector");
            return;
        }
        canvas =  setAndAddCanvas(parentEl);
        ctx  = canvas.getContext("2d");
        self.dataNormalized = normalizeData(data);
        drawBubbles();
    };
    function drawBubbles() {
            console.log(self.dataNormalized)
        for (var i in self.dataNormalized) {
            var oData = dataNormalized[i];
            ctx.beginPath();
            ctx.arc( (360-oData.days2Renew)*xMultipier,
                oData.daysFromLContact*yMultipier,
                oData.payingAmount*2,
                0, 2 * Math.PI, false);
            var color = 'green';
            if( oData.health <= 7.5 )
                color = 'yellow';
            if(oData.health <= 3.5)
                color = 'red';
            ctx.fillStyle = color;
            ctx.fill();
        }
    }
    function normalizeData(data) {
    var ret =  [];
    for (var i = 0 ; i< data.length ; i++){
        var oObj = {$index:i},
        oData = data[i];
        oObj.days2Renew = 0-  getDaysCount(oData.lastRenewal+ (1000*3600*24*360));
        oObj.daysFromLContact = getDaysCount(oData.lastContact);
        oObj.health = oData.health;
        oObj.payingAmount = oData.payingAmount;
        ret.push(oObj);
    }
    return ret;
    }
    function getDaysCount(to,from) {
        if(!from){
            from = new Date();
        }
       var diff = new Date(from).getTime() - new Date(to).getTime()
        return Math.ceil( diff / (1000*3600*24) )
    }
    function setAndAddCanvas(parentEl) {
        var canvas = document.createElement("canvas");
        canvas.width = 360 * xMultipier;
        canvas.height = 90 * yMultipier;
        parentEl.appendChild(canvas);
        return canvas;
    }
    self.generateRandomData = function (count) {
    var ret = [];
        for (var i = 0 ; i < count; i ++){
            ret.push({
                "lastRenewal": new Date().getTime() - getRandom(360)*(1000*3600*24) ,
                "lastContact":new Date().getTime() - getRandom(90)*(1000*3600*24) ,
                "payingAmount": getRandom(20),
                "health": getRandom(10)
            })
        }
        return ret;
        function getRandom(max) {
          return   Math.round(Math.random() * max)
        }
    };
    return self;
})();