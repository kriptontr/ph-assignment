const app = require('express')();
const express = require('express');
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const {uuid} = require('uuidv4');
var level = require('level');
var db = level('high-score');
let connections = [],
    sessions = [];
io.on('connection', function (socket) {
    connections.push(socket);
    readScores(10, socket);
    socket.on("disconnect", function () {
        let i = connections.indexOf(socket);
        connections.splice(i, 1);
    });
    socket.on("start-session", function (data) {
        var id = uuid();
        sessions.push({
            id: id,
            socket: socket,
            name: data.name,
            startTime: new Date()
        });
        socket.emit("new-session", {id: id})
    })
    socket.on("score", function (data) {
        var session = sessions.filter(a => {
            return a.id === data.sessionID;
        })
        if (session.length > 0) {
            session[0].score = data.args;
        }
        io.sockets.emit("upsert-now-playing", {name: session.name, score: session.score})
    });
    socket.on("end-session", function (data) {
        var session = sessions.filter(a => {
            return a.id === data.sessionID;
        });

        if (session.length > 0) {
            addScore(session[0]);
            sessions.splice(sessions.indexOf(session[0]), 1);
        }
    })
});

function addScore(data) {
    delete data.socket;
    db.put(data.score + "-" + data.name, JSON.stringify(data))
}

function readScores(count, socket) {
    let i = 0;
    let stream = db.createReadStream({keys: false, values: true})
        .on('data', function (data) {
            socket.emit("upsert-high-score", JSON.parse(data))
            i++;
            if (i > count) {
                stream.close();
                return;
            }
        })
}

app.use(express.static("./F"));
http.listen(3000, function () {
    console.log('listening on *:3000');
});
