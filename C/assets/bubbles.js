window.bubbles = (function () {
    var self = this;
    var xMultipier = 2,
        yMultipier = 3,
        radiusMultipier = 2;
    var canvas, ctx;
    self.init = function (selector, data) {
        let parentEl = document.querySelector(selector);
        if (!parentEl) {
            alert("Cant find container element described in init selector");
            return;
        }
        canvas = setAndAddCanvas(parentEl);
        canvas.addEventListener("click", onclick, false)
        ctx = canvas.getContext("2d");
        self.dataNormalized = normalizeData(data);
        drawBubbles();

    };

    function onclick(event) {
        let snapShot = Object.assign({}, self.dataNormalized)
        for (let i in snapShot) {
            let oData = snapShot[i];
            let x2 = Math.pow(Math.abs(event.layerX - ((360 - oData.days2Renew) * xMultipier)), 2),
                y2 = Math.pow(Math.abs(event.layerY - (oData.daysFromLContact * yMultipier)), 2);
            let hypo = Math.round(Math.sqrt(x2 + y2));
            if (oData.payingAmount * radiusMultipier > hypo) {
                let collapsed = self.dataNormalized.filter(function (a) {
                    return a.$index === oData.$index
                })
                if (collapsed.length > 0) {
                    collapsed[0].daysFromLContact = 0;
                    break;
                    return;
                }
            }

        }
    }

    self.animateBubbles = function (seconds, fps) {
        console.log("animation started" + new Date());
        //TODO miliseconds spent while calculating and  drawing bubbles can be calculated and  reduced from timeForFrame each iteration for more precise timing.
        let timeForFrame = Math.round(1000 / fps),
            drawnFrames = 0,
            framesToDraw = (seconds * 1000) / timeForFrame;
        let intID = setInterval(function () {
            if (drawnFrames > framesToDraw) {
                console.log("animation Done" + new Date());
                clearInterval(intID)
            }
            drawnFrames++;
            calculateData(timeForFrame);
            drawBubbles();
        }, timeForFrame)
    };

    function calculateData(waitTimeMs) {
        let daysPassed = (waitTimeMs / 1000) * 6;
        for (let i in self.dataNormalized) {
            let oData = dataNormalized[i];
            oData.days2Renew = oData.days2Renew - daysPassed;
            if (oData.days2Renew < 0) {
                oData.days2Renew = 360
            }
            oData.daysFromLContact = oData.daysFromLContact + daysPassed;
            if (oData.daysFromLContact > 90) {
                self.dataNormalized.splice(i, 1)
            }
        }
    }

    function drawBubbles() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        // console.log(self.dataNormalized);
        for (let i in self.dataNormalized) {
            let oData = dataNormalized[i];
            ctx.beginPath();
            ctx.arc((360 - oData.days2Renew) * xMultipier,
                oData.daysFromLContact * yMultipier,
                oData.payingAmount * radiusMultipier,
                0, 2 * Math.PI, false);
            let color = 'green';
            if (oData.health <= 7.5)
                color = 'yellow';
            if (oData.health <= 3.5)
                color = 'red';
            ctx.fillStyle = color;
            ctx.fill();
        }
    }

    function normalizeData(data) {
        var ret = [];
        for (let i = 0; i < data.length; i++) {
            let oObj = {$index: i},
                oData = data[i];
            oObj.days2Renew = 0 - getDaysCount(oData.lastRenewal + (1000 * 3600 * 24 * 360));
            oObj.daysFromLContact = getDaysCount(oData.lastContact);
            oObj.health = oData.health;
            oObj.payingAmount = oData.payingAmount;
            ret.push(oObj);
        }
        return ret;
    }

    function getDaysCount(to, from) {
        if (!from) {
            from = new Date();
        }
        let diff = new Date(from).getTime() - new Date(to).getTime()
        return Math.ceil(diff / (1000 * 3600 * 24))
    }

    function setAndAddCanvas(parentEl) {
        var canvas = document.createElement("canvas");
        canvas.width = 360 * xMultipier;
        canvas.height = 90 * yMultipier;
        parentEl.appendChild(canvas);
        return canvas;
    }

    self.generateRandomData = function (count) {
        let ret = [];
        for (let i = 0; i < count; i++) {
            ret.push({
                "lastRenewal": new Date().getTime() - getRandom(360) * (1000 * 3600 * 24),
                "lastContact": new Date().getTime() - getRandom(90) * (1000 * 3600 * 24),
                "payingAmount": getRandom(20),
                "health": getRandom(10)
            })
        }
        return ret;

        function getRandom(max) {
            return Math.round(Math.random() * max)
        }
    };
    return self;
})();